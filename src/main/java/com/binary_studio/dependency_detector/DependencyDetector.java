package com.binary_studio.dependency_detector;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public final class DependencyDetector {

  private DependencyDetector() {
  }

  public static boolean canBuild(DependencyList libraries) {
    return libraries.libraries.stream().noneMatch(lib -> {
      Set<String> firstLevel = getFirstLevelDeps(lib, libraries);
      if (findCyclicDependency(firstLevel, lib)) {
        return true;
      }
      Set<String> nextLevel = getNextLevelDeps(firstLevel, libraries);
      while (!nextLevel.isEmpty()) {
        if (findCyclicDependency(nextLevel, lib)) {
          return true;
        }
        nextLevel = getNextLevelDeps(nextLevel, libraries);
      }
      return false;
    });
  }

  private static Set<String> getFirstLevelDeps(String lib, DependencyList libraries) {
    return libraries.dependencies.stream().filter(strings -> strings[0]
        .equals(lib)).map(strings -> strings[1]).collect(Collectors.toSet());
  }

  private static Set<String> getNextLevelDeps(Set<String> dependencies, DependencyList libraries) {
    Set<String> nextLevelDeps = new HashSet<>();
    dependencies.forEach(s -> nextLevelDeps.addAll(getFirstLevelDeps(s, libraries)));
    return nextLevelDeps;
  }

  private static boolean findCyclicDependency(Set<String> nextLevelDeps, String lib) {
    if (nextLevelDeps.isEmpty()) {
      return false;
    }
    return nextLevelDeps.contains(lib);
  }
}
