package com.binary_studio.fleet_commander.core.ship;

import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.exceptions.InsufficientPowergridException;
import com.binary_studio.fleet_commander.core.exceptions.NotAllSubsystemsFitted;
import com.binary_studio.fleet_commander.core.ship.contract.ModularVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class DockedShip implements ModularVessel {

  private final String name;
  private final PositiveInteger shieldHP;
  private final PositiveInteger hullHP;
  private final PositiveInteger powergridOutput;
  private PositiveInteger usedPG;
  private final PositiveInteger capacitorAmount;
  private final PositiveInteger capacitorRechargeRate;
  private final PositiveInteger speed;
  private final PositiveInteger size;

  private boolean hasWeapon;
  private boolean hasDefencive;

  private AttackSubsystem weapon;
  private DefenciveSubsystem defencive;

  private DockedShip(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
      PositiveInteger powergridOutput,
      PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate, PositiveInteger speed,
      PositiveInteger size) {
    this.name = name;
    this.shieldHP = shieldHP;
    this.hullHP = hullHP;
    this.powergridOutput = powergridOutput;
    usedPG = PositiveInteger.of(0);
    this.capacitorAmount = capacitorAmount;
    this.capacitorRechargeRate = capacitorRechargeRate;
    this.speed = speed;
    this.size = size;

    hasWeapon = false;
    hasDefencive = false;
  }

  public static DockedShip construct(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
      PositiveInteger powergridOutput, PositiveInteger capacitorAmount,
      PositiveInteger capacitorRechargeRate,
      PositiveInteger speed, PositiveInteger size) {
    if (name.isBlank()) {
      throw new IllegalArgumentException("Name should be not null and not empty");
    }
    return new DockedShip(name, shieldHP, hullHP,
        powergridOutput, capacitorAmount, capacitorRechargeRate,
        speed, size);
  }

  @Override
  public void fitAttackSubsystem(AttackSubsystem subsystem) throws InsufficientPowergridException {
    if (subsystem == null) {
      if (hasWeapon) {
        withdrawAttackSubsystem();
      }
    } else if (!hasWeapon) {
      addAttackSubsystem(subsystem);
    } else {
      reFitAttackSubSystem(subsystem);
    }
  }

  private void addAttackSubsystem(AttackSubsystem subsystem) throws InsufficientPowergridException {
    int freePG = powergridOutput.value() - usedPG.value();
    if (subsystem.getPowerGridConsumption().value() > freePG) {
      throw new InsufficientPowergridException(
          subsystem.getPowerGridConsumption().value() - freePG);
    }
    hasWeapon = true;
    usedPG = PositiveInteger.of(usedPG.value() + subsystem.getPowerGridConsumption().value());
    weapon = subsystem;
  }

  private void withdrawAttackSubsystem() {
    hasWeapon = false;
    usedPG = PositiveInteger.of(usedPG.value() - weapon.getPowerGridConsumption().value());
    weapon = null;
  }

  private void reFitAttackSubSystem(AttackSubsystem subsystem)
      throws InsufficientPowergridException {
    int pGCanBeFree =
        powergridOutput.value() - usedPG.value() + weapon.getPowerGridConsumption().value();
    if (pGCanBeFree < subsystem.getPowerGridConsumption().value()) {
      throw new InsufficientPowergridException(
          subsystem.getPowerGridConsumption().value() - pGCanBeFree);
    }
    withdrawAttackSubsystem();
    addAttackSubsystem(subsystem);
  }

  @Override
  public void fitDefensiveSubsystem(DefenciveSubsystem subsystem)
      throws InsufficientPowergridException {
    if (subsystem == null) {
      if (hasDefencive) {
        withdrawDefenciveSubsystem();
      }
    } else if (!hasDefencive) {
      addDefenciveSubsystem(subsystem);
    } else {
      reFitDefenciveSubsystem(subsystem);
    }
  }

  private void addDefenciveSubsystem(DefenciveSubsystem subsystem)
      throws InsufficientPowergridException {
    int freePG = powergridOutput.value() - usedPG.value();
    if (subsystem.getPowerGridConsumption().value() > freePG) {
      throw new InsufficientPowergridException(
          subsystem.getPowerGridConsumption().value() - freePG);
    } else {
      hasDefencive = true;
      usedPG = PositiveInteger.of(usedPG.value() + subsystem.getPowerGridConsumption().value());
      defencive = subsystem;
    }
  }

  private void withdrawDefenciveSubsystem() {
    if (hasDefencive) {
      hasDefencive = false;
      usedPG = PositiveInteger.of(usedPG.value() - defencive.getPowerGridConsumption().value());
      defencive = null;
    }
  }

  private void reFitDefenciveSubsystem(DefenciveSubsystem subsystem)
      throws InsufficientPowergridException {
    int pGCanBeFree =
        powergridOutput.value() - usedPG.value() + defencive.getPowerGridConsumption().value();
    if (pGCanBeFree < subsystem.getPowerGridConsumption().value()) {
      throw new InsufficientPowergridException(
          subsystem.getPowerGridConsumption().value() - pGCanBeFree);
    }
    withdrawDefenciveSubsystem();
    addDefenciveSubsystem(subsystem);
  }

  public CombatReadyShip undock() throws NotAllSubsystemsFitted {
    if (!hasDefencive && !hasWeapon) {
      throw NotAllSubsystemsFitted.bothMissing();
    } else if (!hasWeapon) {
      throw NotAllSubsystemsFitted.attackMissing();
    } else if (!hasDefencive) {
      throw NotAllSubsystemsFitted.defenciveMissing();
    }
    return new CombatReadyShip(name, shieldHP, hullHP, powergridOutput, usedPG, capacitorAmount,
        capacitorRechargeRate, speed, size, weapon, defencive);
  }
}
