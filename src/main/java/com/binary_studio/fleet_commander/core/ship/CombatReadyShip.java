package com.binary_studio.fleet_commander.core.ship;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.AttackResult;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.ship.contract.CombatReadyVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;
import java.util.Optional;

public final class CombatReadyShip implements CombatReadyVessel {

  final private PositiveInteger startShieldHP;
  final private PositiveInteger startHullHp;
  final private PositiveInteger startCapacitorAmount;
  private final String name;
  private PositiveInteger shieldHP;
  private PositiveInteger hullHP;
  private final PositiveInteger powergridOutput;
  private final PositiveInteger usedPG;
  private PositiveInteger capacitorAmount;
  private final PositiveInteger capacitorRechargeRate;
  private PositiveInteger speed;
  private final PositiveInteger size;
  private final AttackSubsystem weapon;
  private final DefenciveSubsystem defencive;

  public CombatReadyShip(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
      PositiveInteger powergridOutput, PositiveInteger usedPG, PositiveInteger capacitorAmount,
      PositiveInteger capacitorRechargeRate, PositiveInteger speed, PositiveInteger size,
      AttackSubsystem weapon, DefenciveSubsystem defencive) {
    this.name = name;
    this.shieldHP = shieldHP;
    startShieldHP = shieldHP;
    this.hullHP = hullHP;
    startHullHp = hullHP;
    this.powergridOutput = powergridOutput;
    this.usedPG = usedPG;
    this.capacitorAmount = capacitorAmount;
    startCapacitorAmount = capacitorAmount;
    this.capacitorRechargeRate = capacitorRechargeRate;
    this.speed = speed;
    this.size = size;
    this.weapon = weapon;
    this.defencive = defencive;
  }

  @Override
  public void endTurn() {
    capacitorAmount = PositiveInteger.of(
        Math.min(startCapacitorAmount.value(),
            capacitorAmount.value() + capacitorRechargeRate.value()));
  }

  @Override
  public void startTurn() {
  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public PositiveInteger getSize() {
    return size;
  }

  @Override
  public PositiveInteger getCurrentSpeed() {
    return speed;
  }

  @Override
  public Optional<AttackAction> attack(Attackable target) {
    if (weapon.getCapacitorConsumption().value() > capacitorAmount.value()) {
      return Optional.empty();
    }
    capacitorAmount = PositiveInteger
        .of(capacitorAmount.value() - weapon.getCapacitorConsumption().value());
    return Optional.of(new AttackAction(weapon.attack(target), this, target, weapon));
  }

  @Override
  public AttackResult applyAttack(AttackAction attack) {
    AttackAction reducedAttack = defencive.reduceDamage(attack);
    if (shieldHP.value() >= reducedAttack.damage.value()) {
      shieldHP = PositiveInteger.of(shieldHP.value() - reducedAttack.damage.value());
    } else {
      int hullDamage = reducedAttack.damage.value() - shieldHP.value();
      if (hullDamage > hullHP.value()) {
        return new AttackResult.Destroyed();
      } else {
        hullHP = PositiveInteger.of(hullHP.value() - hullDamage);
        shieldHP = PositiveInteger.of(0);
      }
    }
    return new AttackResult.DamageRecived(weapon, reducedAttack.damage, this);
  }

  @Override
  public Optional<RegenerateAction> regenerate() {
    if (capacitorAmount.value() < defencive.getCapacitorConsumption().value()) {
      return Optional.empty();
    }
    capacitorAmount = PositiveInteger
        .of(capacitorAmount.value() - defencive.getCapacitorConsumption().value());
    RegenerateAction canRegen = defencive.regenerate();

    PositiveInteger shieldLess = PositiveInteger.of(startShieldHP.value() - shieldHP.value());
    PositiveInteger hullLess = PositiveInteger.of(startHullHp.value() - hullHP.value());

    PositiveInteger shieldRegen = shieldLess.value() == 0 ? PositiveInteger.of(0) :
        shieldLess.value() > canRegen.shieldHPRegenerated.value() ? canRegen.shieldHPRegenerated
            : shieldLess;
    PositiveInteger hullRegen = hullLess.value() == 0 ? PositiveInteger.of(0) :
        hullLess.value() > canRegen.hullHPRegenerated.value() ? canRegen.hullHPRegenerated
            : hullLess;
    return Optional.of(new RegenerateAction(shieldRegen, hullRegen));
  }
}
