package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class DefenciveSubsystemImpl implements DefenciveSubsystem {

  PositiveInteger capacitorConsumption;
  PositiveInteger impactReductionPercent;
  PositiveInteger shieldRegeneration;
  PositiveInteger hullRegeneration;
  private final String name;
  private final PositiveInteger powergridConsumption;

  private DefenciveSubsystemImpl(String name, PositiveInteger powergridConsumption,
      PositiveInteger capacitorConsumption, PositiveInteger impactReductionPercent,
      PositiveInteger shieldRegeneration, PositiveInteger hullRegeneration) {
    this.name = name;
    this.powergridConsumption = powergridConsumption;
    this.capacitorConsumption = capacitorConsumption;
    this.impactReductionPercent = impactReductionPercent;
    this.shieldRegeneration = shieldRegeneration;
    this.hullRegeneration = hullRegeneration;
  }

  public static DefenciveSubsystemImpl construct(String name, PositiveInteger powergridConsumption,
      PositiveInteger capacitorConsumption, PositiveInteger impactReductionPercent,
      PositiveInteger shieldRegeneration, PositiveInteger hullRegeneration)
      throws IllegalArgumentException {
    if (name == null || name.isBlank()) {
      throw new IllegalArgumentException("Name should be not null and not empty");
    }
    return new DefenciveSubsystemImpl(name, powergridConsumption, capacitorConsumption,
        impactReductionPercent,
        shieldRegeneration, hullRegeneration);
  }

  @Override
  public PositiveInteger getPowerGridConsumption() {
    return powergridConsumption;
  }

  @Override
  public PositiveInteger getCapacitorConsumption() {
    return capacitorConsumption;
  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public AttackAction reduceDamage(AttackAction incomingDamage) {
    double realReductionPercent =
        impactReductionPercent.value() < 95 ? impactReductionPercent.value() : 95;
    PositiveInteger reducedDamage = PositiveInteger.of(
        (int) Math.ceil(incomingDamage.damage.value()
            - incomingDamage.damage.value() * realReductionPercent / 100));
    return new AttackAction(reducedDamage, incomingDamage.attacker, incomingDamage.target,
        incomingDamage.weapon);
  }

  @Override
  public RegenerateAction regenerate() {
    return new RegenerateAction(shieldRegeneration, hullRegeneration);
  }

}
