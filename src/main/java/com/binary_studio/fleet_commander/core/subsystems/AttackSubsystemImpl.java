package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;

public final class AttackSubsystemImpl implements AttackSubsystem {

  private final String name;
  private final PositiveInteger baseDamage;
  private final PositiveInteger optimalSize;
  private final PositiveInteger optimalSpeed;
  private final PositiveInteger capacitorConsumption;
  private final PositiveInteger pgRequirement;

  private AttackSubsystemImpl(String name, PositiveInteger powergridRequirments,
      PositiveInteger capacitorConsumption, PositiveInteger optimalSpeed,
      PositiveInteger optimalSize,
      PositiveInteger baseDamage) {
    this.name = name;
    pgRequirement = powergridRequirments;
    this.capacitorConsumption = capacitorConsumption;
    this.optimalSpeed = optimalSpeed;
    this.optimalSize = optimalSize;
    this.baseDamage = baseDamage;
  }

  public static AttackSubsystemImpl construct(String name, PositiveInteger powergridRequirments,
      PositiveInteger capacitorConsumption, PositiveInteger optimalSpeed,
      PositiveInteger optimalSize,
      PositiveInteger baseDamage) throws IllegalArgumentException {
    if (name == null || name.isBlank()) {
      throw new IllegalArgumentException("Name should be not null and not empty");
    }

    return new AttackSubsystemImpl(name, powergridRequirments,
        capacitorConsumption, optimalSpeed, optimalSize,
        baseDamage);
  }

  @Override
  public PositiveInteger getPowerGridConsumption() {
    return pgRequirement;
  }

  @Override
  public PositiveInteger getCapacitorConsumption() {
    return capacitorConsumption;
  }

  @Override
  public PositiveInteger attack(Attackable target) {
    double sizeReductionModifier = target.getSize().value() >= optimalSize.value() ?
        1 : (double) target.getSize().value() / optimalSize.value();
    double speedReductionModifier = target.getCurrentSpeed().value() <= optimalSpeed.value() ?
        1 : (double) optimalSpeed.value() / (2 * target.getCurrentSpeed().value());
    return PositiveInteger.of(
        (int) Math
            .ceil(baseDamage.value() * Math.min(sizeReductionModifier, speedReductionModifier)));
  }

  @Override
  public String getName() {
    return name;
  }
}
