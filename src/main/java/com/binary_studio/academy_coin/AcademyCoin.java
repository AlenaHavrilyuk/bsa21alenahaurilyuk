package com.binary_studio.academy_coin;

import java.util.Arrays;
import java.util.stream.Stream;

public final class AcademyCoin {

  private AcademyCoin() {
  }

  public static int maxProfit(Stream<Integer> prices) {
    int[] input = prices.mapToInt(Integer::intValue).toArray();
    if (input.length == 0) {
      return 0;
    }
    // profit from one transaction, if buy at day (j + 1) and sell at day (i + 2)
    int[][] transProfit = new int[input.length - 1][];
    for (int i = 0; i < transProfit.length; i++) {
      for (int j = 0; j < transProfit.length - i; j++) {
        transProfit[i] = new int[transProfit.length - i];
        transProfit[i][j] = Math.max(input[input.length - j - 1] - input[i], 0);
      }
    }
    // max profit you can get, if you buy coin last time at day (j + 1) and finish selling at day (i + 2)
    int[][] maxProfit = new int[input.length - 1][];
    for (int i = maxProfit.length - 1; i >= 0; i--) {
      for (int j = 0; j < maxProfit.length - i; j++) {
        maxProfit[i] = new int[maxProfit.length - i];
        if (j == 0) {
          maxProfit[i][j] = transProfit[i][j];
          continue;
        }
        if (j == maxProfit[i].length - 1) {
          maxProfit[i][j] = maxProfit[i + 1][j - 1] + transProfit[i][j];
          continue;
        }
        maxProfit[i][j] = Math
            .max(maxProfit[i + 1][j], (maxProfit[i + 1][j - 1] + transProfit[i][j]));
      }
    }
    return Arrays.stream(maxProfit[0]).max().orElse(0);
  }
}
